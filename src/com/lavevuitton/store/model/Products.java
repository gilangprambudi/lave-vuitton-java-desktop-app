package com.lavevuitton.store.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lavevuitton.store.DataObject.ProductObject;
import com.lavevuitton.store.system.MySQLConnection;

public class Products {
	
	MySQLConnection sqlConnection;
	Statement statement;
	
	boolean connectionOpen;
	
	
	public Products() {
		sqlConnection = new MySQLConnection();
		
		
		
	}
	
	public ProductObject getProduct(String ProductID) {
		sqlConnection.openConnection();
		ProductObject product = new ProductObject();
		String sql = "SELECT * FROM products WHERE ProductID = ?";
		
		try {
			PreparedStatement ps = sqlConnection.conn.prepareStatement(sql);
			ps.setString(1, ProductID);
			ResultSet rs = ps.executeQuery();
			
			
			if(rs.next()) {	
            	product.ProductID	= rs.getString("ProductID");
            	product.ProductName = rs.getString("ProductName");
            	product.ProductPrice= rs.getInt("ProductPrice");
            	product.ProductColor= rs.getString("ProductColor");
            	product.ProductQty	= rs.getInt("ProductQty");
            }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		sqlConnection.closeConnection();
		return product;
	}
	
	public ProductObject insertProduct(ProductObject product) {
		sqlConnection.openConnection();
		
		String sql = "SELECT ProductID FROM products";
		int size= 0;
		String ProductID = "";
		try {
			statement = sqlConnection.conn.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			if (rs != null) {
			  rs.last();
			  
			  size = Integer.valueOf(rs.getString("ProductID").substring(2)) + 1; 
			  if(rs.getRow() > size) {
				  size = rs.getRow() + 1;
			  }
			}
			
			ProductID = "PD"+String.format("%03d", size);
			rs.close();
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(!ProductID.equals("")) {
			sql = "INSERT INTO products(ProductID,ProductName,ProductPrice,ProductColor,ProductQty)"
		            + "VALUES(?,?,?,?,?)";
		
			try {
				PreparedStatement pstmt = sqlConnection.conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, ProductID);
				pstmt.setString(2, product.ProductName);
				pstmt.setInt(3, product.ProductPrice);
				pstmt.setString(4, product.ProductColor);
				pstmt.setInt(5, product.ProductQty);
				
				
				int rowAffected = pstmt.executeUpdate();
				if(rowAffected == 1)
				{
					product.ProductID = ProductID;
				 
				}else {
					product = null;
				}
				pstmt.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		sqlConnection.closeConnection();
			
		return product;
	}
		
	public ArrayList<ProductObject> getAllProducts() {
		sqlConnection.openConnection();
		ArrayList<ProductObject> productList = new ArrayList<>();
		
		try {
			statement = sqlConnection.conn.createStatement();
			String sql = "SELECT * FROM products";
			ResultSet rs = statement.executeQuery(sql);
			ProductObject product;
            while (rs.next()) {
            	product = new ProductObject();
            	product.ProductID	= rs.getString("ProductID");
            	product.ProductName = rs.getString("ProductName");
            	product.ProductPrice= rs.getInt("ProductPrice");
            	product.ProductColor= rs.getString("ProductColor");
            	product.ProductQty	= rs.getInt("ProductQty");
            	productList.add(product);
            }
            
            rs.close();
            statement.close();
            
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sqlConnection.closeConnection();
		return productList;
	}
	
	
	public Integer addOrUpdateProduct(ProductObject product) {
		sqlConnection.openConnection();
		Integer productStatus = 0;
		
		String productName = product.ProductName;
		String productColor = product.ProductColor;
		System.out.println(productName);
		String sql = "SELECT * FROM products WHERE ProductName = ? AND ProductColor = ?";
		int size = 0;			
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement =sqlConnection.conn.prepareStatement(sql);
			preparedStatement.setString(1, productName);
			preparedStatement.setString(2, productColor);
			
			ResultSet rs	      = preparedStatement.executeQuery();
			if (rs != null) {
				  while(rs.next()) {
					  product.ProductID = rs.getString("ProductID");
					  size++;
				  } 
			}
			
			
			if(size == 0) {
				
				product = this.insertProduct(product);
				
				if(product.ProductID != null) {
					return 1;
				}else {
					return 0;
				}
			}else {
				product = this.updateProduct(product);
				
				if(product.ProductID != null) {
					return 2;
				}else {
					return 0;
				}
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			preparedStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sqlConnection.closeConnection();
		return productStatus;
	}
	
	
	public ProductObject updateProduct(ProductObject product) {
		sqlConnection.openConnection();
		String sql = "UPDATE products SET ProductName = ?, ProductColor = ?, ProductPrice = ?, ProductQty = ? WHERE ProductID = ?";
	
		try {
			PreparedStatement pstmt = sqlConnection.conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, product.ProductName);
			pstmt.setString(2, product.ProductColor);
			pstmt.setInt(3, product.ProductPrice);
			pstmt.setInt(4, product.ProductQty);
			pstmt.setString(5, product.ProductID);
			
			
			int rowAffected = pstmt.executeUpdate();
			if(rowAffected == 1)
			{
				
			 
			}else {
				product = null;
			}
			pstmt.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		sqlConnection.closeConnection();
		return product; 
	}
	
	
	//TODO
	public boolean deleteProduct(ProductObject product) {
		sqlConnection.openConnection();
		boolean productStatus = false;
		
		String sql = "DELETE FROM products WHERE ProductID = ?";
		
		try {
			PreparedStatement pstmt = sqlConnection.conn.prepareStatement(sql);
			pstmt.setString(1, product.ProductID);
			int row = pstmt.executeUpdate();
			if(row > 0) {
				productStatus = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		sqlConnection.closeConnection();
		return productStatus;
	}
	
}

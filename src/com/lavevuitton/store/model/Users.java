package com.lavevuitton.store.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;

import com.lavevuitton.store.DataObject.UserObject;
import com.lavevuitton.store.system.MySQLConnection;

public class Users {
	MySQLConnection sqlConnection;
	Statement statement;
	ResultSet rs;
	boolean connectionOpen;
	
	
	public Users() {
		sqlConnection = new MySQLConnection();
		
		
		
	}
	
	
	public boolean updateUser(UserObject user) {
		sqlConnection.openConnection();
		boolean userUpdated = false;
		
		String sql = "UPDATE users SET UserGender = ?, UserAddress=?, UserPassword=? WHERE UserID = ?";
		
		try {
			PreparedStatement ps = sqlConnection.conn.prepareStatement(sql);
			ps.setString(1, user.UserGender);
			ps.setString(2,  user.UserAddress);
			ps.setString(3, user.UserPassword);
			ps.setString(4, user.UserID);;
			int row = ps.executeUpdate();
			
			if(row > 0) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sqlConnection.closeConnection();
		return userUpdated;
	}
	public UserObject insertUser(UserObject user) {
		sqlConnection.openConnection();
		
		String sql = "SELECT UserID FROM users";
		int size= 0;
		String UserID = "";
		try {
			statement = sqlConnection.conn.createStatement();
			rs = statement.executeQuery(sql);
			if (rs != null) {
				  rs.last();
				  
				  size = Integer.valueOf(rs.getString("UserID").substring(2)) + 1; 
				  if(rs.getRow() > size) {
					  size = rs.getRow() + 1;
				  }
			}
			
			UserID = "US"+String.format("%03d", size);
			rs.close();
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(!UserID.equals("")) {
			sql = "INSERT INTO users(UserID,UserEmail,UserPassword,UserDOB,UserGender, UserAddress, UserRole)"
		            + "VALUES(?,?,?,?,?,?,?)";
		
			try {
				PreparedStatement pstmt = sqlConnection.conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				pstmt.setString(1, UserID);
				pstmt.setString(2, user.UserEmail);
				pstmt.setString(3, user.UserPassword);
				pstmt.setDate(4, new java.sql.Date(user.UserDOB.getTime() + TimeUnit.DAYS.toMillis( 1 )));
				pstmt.setString(5, user.UserGender);
				pstmt.setString(6, user.UserAddress);
				pstmt.setString(7, user.UserRole);
				
				int rowAffected = pstmt.executeUpdate();
				if(rowAffected == 1)
				{
				  user.UserID = UserID;
				  user.UserRole = "customer";
				}else {
					user = null;
				}
				pstmt.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		sqlConnection.closeConnection();
			
		return user;
	}
	
	public UserObject getUser(String UserID) {
		sqlConnection.openConnection();
		UserObject user = new UserObject();
		try {
			statement = sqlConnection.conn.createStatement();
			String sql = "SELECT * FROM users WHERE UserID='"+UserID+"'";
			rs = statement.executeQuery(sql);
			
            while (rs.next()) {
            	user.UserID 	 	= UserID;
    			user.UserEmail 	 	= rs.getString("UserEmail");
    			user.UserGender  	= rs.getString("UserGender");
    			user.UserAddress 	= rs.getString("UserAddress");
    			user.UserPassword  	= rs.getString("UserPassword");
                user.UserDOB 		= rs.getDate("UserDOB");
            }
            
            rs.close();
            statement.close();
            
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sqlConnection.closeConnection();
		return user;
	}
	
	public UserObject getUserByEmail(String Email) {
		sqlConnection.openConnection();
		UserObject user = new UserObject();
		try {
			statement = sqlConnection.conn.createStatement();
			String sql = "SELECT * FROM users WHERE UserEmail='"+Email+"'";
			rs = statement.executeQuery(sql);
			
            while (rs.next()) {
            	user.UserID 	 	= rs.getString("UserID");
    			user.UserEmail 	 	= rs.getString("UserEmail");
    			user.UserGender  	= rs.getString("UserGender");
    			user.UserAddress 	= rs.getString("UserAddress");
    			user.UserPassword  	= rs.getString("UserPassword");
                user.UserDOB 		= rs.getDate("UserDOB");
                user.UserRole		= rs.getString("UserRole");
            }
            
            rs.close();
            statement.close();
            
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sqlConnection.closeConnection();
		return user;
	}
	
	
}

package com.lavevuitton.store.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lavevuitton.store.DataObject.CartObject;
import com.lavevuitton.store.DataObject.ProductObject;
import com.lavevuitton.store.DataObject.UserObject;
import com.lavevuitton.store.system.MySQLConnection;

public class Carts {
	
	MySQLConnection sqlConnection;
	public Carts() {
		sqlConnection = new MySQLConnection();
	}
	
	
	public CartObject getCart(String UserID, String ProductID) {
		sqlConnection.openConnection();
		CartObject cart = new CartObject();
		String sql = "SELECT * FROM my_cart WHERE ProductID = ? AND UserID = ?";
		
		try {
			PreparedStatement ps = sqlConnection.conn.prepareStatement(sql);
			ps.setString(1, ProductID);
			ps.setString(2, UserID);
			ResultSet rs = ps.executeQuery();
			
			
			if(rs.next()) {	
				cart.UserID = UserID;
				Products productM = new Products();
				ProductObject product = productM.getProduct(ProductID);
				product.ProductQty = rs.getInt("ProductQty");
				cart.Product = product;
            }
			rs.close();
			ps.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		sqlConnection.closeConnection();
		return cart;
	}
	
	public ArrayList<CartObject> getCartByUserID(String UserID) {
		sqlConnection.openConnection();
		ArrayList<CartObject> carts = new ArrayList<CartObject>();
		String sql = "SELECT * FROM my_cart WHERE UserID = ?";
		
		try {
			PreparedStatement ps = sqlConnection.conn.prepareStatement(sql);
			ps.setString(1, UserID);
			ResultSet rs = ps.executeQuery();
			
			
			while(rs.next()) {
				CartObject cart = new CartObject();
				cart.UserID = UserID;
				
				Products productM = new Products();
				ProductObject product = productM.getProduct(rs.getString("ProductID"));
				product.ProductQty = rs.getInt("ProductQty");
				cart.Product = product;
				carts.add(cart);
            }
			rs.close();
			ps.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		sqlConnection.closeConnection();
		return carts;
	}
	
	
	public CartObject insertCart(UserObject user, ProductObject product) {
		sqlConnection.openConnection();
		
		CartObject cart = new CartObject();
		String sql = "INSERT INTO my_cart(UserID,ProductID,ProductQty) VALUES(?,?,?)";
	
		try {
			PreparedStatement pstmt = sqlConnection.conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, user.UserID);
			pstmt.setString(2, product.ProductID);
			pstmt.setInt(3, product.ProductQty);
			
			int rowAffected = pstmt.executeUpdate();
			if(rowAffected == 1)
			{
			  cart.UserID = user.UserID;
			  cart.Product = product;
			  int cartProductQty = product.ProductQty;
			  
			  Products productM = new Products();
			  product = productM.getProduct(product.ProductID);
			  product.ProductQty = product.ProductQty - cartProductQty;
			  productM.updateProduct(product);
			}else {
				cart = null;
			}
			pstmt.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sqlConnection.closeConnection();
		return cart;
	}
	
	
	public Integer insertOrUpdateCart(UserObject user, ProductObject product) {
		sqlConnection.openConnection();
		Integer cartStatus = 0;
		
		String sql = "SELECT * FROM my_cart WHERE UserID=? AND ProductID = ?";
		PreparedStatement preparedStatement;
		int size = 0;
		
		CartObject cart = new CartObject();
		try {
			preparedStatement =sqlConnection.conn.prepareStatement(sql);
			preparedStatement.setString(1, user.UserID);
			preparedStatement.setString(2, product.ProductID);
			
			ResultSet rs	      = preparedStatement.executeQuery();
			if (rs != null) {
				  while(rs.next()) {
					  product.ProductID = rs.getString("ProductID");
					  size++;
				  } 
			}
			
			
			if(size == 0) {
				
				cart = this.insertCart(user, product);
				
				if(cart.UserID != null) {
					return 1;
				}else {
					return 0;
				}
			}else {
				cart = this.updateCart(user, product);
				
				if(cart.UserID != null) {
					return 2;
				}else {
					return 0;
				}
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sqlConnection.closeConnection();
		
		return cartStatus;
	}
	
	
	public CartObject updateCart(UserObject user, ProductObject product) {
		sqlConnection.openConnection();
		CartObject cart = new CartObject();
		String sql = "UPDATE my_cart SET ProductQty = ? WHERE ProductID = ? AND UserID = ?";
		
		
		CartObject existingCart = this.getCart(user.UserID, product.ProductID);
		sqlConnection.openConnection();
		
		
		int newQty = product.ProductQty + existingCart.Product.ProductQty;
		try {
			PreparedStatement pstmt = sqlConnection.conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, newQty);
			pstmt.setString(3, user.UserID);
			pstmt.setString(2, product.ProductID);
			
			
			int rowAffected = pstmt.executeUpdate();
			if(rowAffected == 1)
			{
				  int cartProductQty = product.ProductQty;
				  
				  Products productM = new Products();
				  product = productM.getProduct(product.ProductID);
				  product.ProductQty = product.ProductQty - cartProductQty;	  
				  productM.updateProduct(product);
				  cart.Product = product;
				  cart.UserID = user.UserID;
			}else {
				cart = null;
			}
			pstmt.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		sqlConnection.closeConnection();
		
		sqlConnection.closeConnection();
		return cart;
	}
	
	public CartObject updateCart2(UserObject user, ProductObject product) {
		sqlConnection.openConnection();
		CartObject cart = new CartObject();
		String sql = "UPDATE my_cart SET ProductQty = ? WHERE ProductID = ? AND UserID = ?";
		
		
		CartObject existingCart = this.getCart(user.UserID, product.ProductID);
		sqlConnection.openConnection();
		
		try {
			PreparedStatement pstmt = sqlConnection.conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, product.ProductQty);
			pstmt.setString(3, user.UserID);
			pstmt.setString(2, product.ProductID);
			
			
			int rowAffected = pstmt.executeUpdate();
			if(rowAffected == 1)
			{
				  int cartProductQty = product.ProductQty;
				  
				  Products productM = new Products();
				  product = productM.getProduct(product.ProductID);
				  System.out.println(cartProductQty);
				  System.out.println(product.ProductQty);
				  System.out.println(existingCart.Product.ProductQty);
				  
				  if(existingCart.Product.ProductQty > cartProductQty) {
					  
					  product.ProductQty = product.ProductQty + (existingCart.Product.ProductQty - cartProductQty);
				  }else {
					  //Cart qty increased;
					  product.ProductQty = product.ProductQty - (cartProductQty- existingCart.Product.ProductQty);
						
				  }
				  
				  productM.updateProduct(product);
				  cart.Product = product;
				  cart.UserID = user.UserID;
			}else {
				cart = null;
			}
			pstmt.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		sqlConnection.closeConnection();
		
		sqlConnection.closeConnection();
		return cart;
	}
	
	public boolean deleteCart(UserObject user, ProductObject product) {
		boolean productDeleted = false;
		
		String sql = "DELETE FROM my_cart WHERE UserID = ? AND ProductID = ?";
		CartObject existingCart = this.getCart(user.UserID, product.ProductID);
		sqlConnection.openConnection();
		try {
			PreparedStatement ps = sqlConnection.conn.prepareStatement(sql);
			ps.setString(1, user.UserID);
			ps.setString(2, product.ProductID);
			int row = ps.executeUpdate();
				
				
				Products productM = new Products();
				ProductObject realProduct = productM.getProduct(product.ProductID);
				realProduct.ProductQty += existingCart.Product.ProductQty;
				productM.updateProduct(realProduct);
				return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		sqlConnection.closeConnection();
		return productDeleted;
	}
}

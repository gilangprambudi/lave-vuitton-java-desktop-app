package com.lavevuitton.store.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lavevuitton.store.DataObject.CartObject;
import com.lavevuitton.store.DataObject.ProductObject;
import com.lavevuitton.store.DataObject.TransactionObject;
import com.lavevuitton.store.DataObject.UserObject;
import com.lavevuitton.store.system.MySQLConnection;

public class Transactions {
	
	MySQLConnection sqlConnection;
	Statement statement;
	ResultSet rs;
	boolean connectionOpen;
	
	public Transactions() {
		sqlConnection = new MySQLConnection();
		
	}
	
	public ArrayList<TransactionObject> getUserTransaction(UserObject user, String status) {
		
		ArrayList<TransactionObject> transactions = new ArrayList<>();
		ArrayList<String> transactionIDList = new ArrayList<>();
		sqlConnection.openConnection();
		String sql = "";
		if(status == null) {
			sql = "SELECT * FROM transaction_header";
		}else{
			sql = "SELECT * FROM transaction_header WHERE TransactionStatus = '"+status+"'";
		}
		
		if(user != null) {
			if(status == null) {
				sql += " WHERE ";
			}else {
				sql += " AND ";
			}
			
			sql += " UserID = '" + user.UserID + "'";
		}
		System.out.println(sql);
		try {
			statement = sqlConnection.conn.createStatement();
			
			rs = statement.executeQuery(sql);
			
			while(rs.next()) {
				transactionIDList.add(rs.getString("TransactionID"));
			}
			
			for(int i = 0; i < transactionIDList.size(); i++) {
				TransactionObject transaction = this.getTransaction(transactionIDList.get(i));
				transactions.add(transaction);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		sqlConnection.closeConnection();
		return transactions;
	}
	
	public TransactionObject getTransaction(String transactionID) {
		TransactionObject transaction = new TransactionObject();
		ArrayList<ProductObject> products = new ArrayList<>();
		sqlConnection.openConnection();
		String sql = "SELECT * FROM transaction_header WHERE TransactionID = '"+transactionID+"'";
		
		try {
			statement = sqlConnection.conn.createStatement();
			
			rs = statement.executeQuery(sql);
			
			if(rs.next()) {
				
				transaction.TransactionID= transactionID;
				transaction.TransactionDate = rs.getDate("TransactionDate");
				transaction.TransactionStatus = rs.getString("TransactionStatus");
				transaction.TransactionRating = rs.getInt("TransactionRating");
				
				sql = "SELECT * FROM transaction_detail WHERE TransactionID ='"+transactionID+"'";
				rs = statement.executeQuery(sql);
				
				Products productM = new Products();
				while(rs.next()) {
					int productQty = rs.getInt("ProductQty");
					ProductObject product = productM.getProduct(rs.getString("ProductID"));
					product.ProductQty = Integer.valueOf(productQty);
					products.add(product);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		transaction.transactionProducts = products;
		return transaction;
	}
	
	public boolean updateTransaction(TransactionObject transaction) {
		boolean isUpdated = false;
		
		return isUpdated;
	}
	
	
	public boolean updateTransactionStatus(String transactionID, int rating) {
		sqlConnection.openConnection();
		boolean statusUpdated = false;
		
		
		String sql = "UPDATE transaction_header SET TransactionStatus = 'success', TransactionRating = "+rating+" WHERE TransactionID = '"+transactionID+"'";
		
		try {
			statement = sqlConnection.conn.createStatement();
			int row = statement.executeUpdate(sql);
			System.out.println("TEST");
			System.out.println(sql);
			if(row > 0) {
				statusUpdated = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sqlConnection.closeConnection();
		return statusUpdated;
	}
	
	
	public ArrayList<ProductObject> getTransactionDetails(String transactionID) {
		ArrayList<ProductObject> transactionDetails = new ArrayList<>();
		sqlConnection.openConnection();
		
		String sql = "SELECT * FROM transaction_detail WHERE TransactionID = "+transactionID;
		try {
			statement = sqlConnection.conn.createStatement();
			try {
				rs = statement.executeQuery(sql);
				ProductObject productNode;
				Products productM = new Products();
				while(rs.next()) {
					int transactionQty = rs.getInt("TransactionQty");
					productNode = productM.getProduct(rs.getString("ProductID"));
					productNode.ProductQty = transactionQty;
					transactionDetails.add(productNode);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		sqlConnection.closeConnection();
		return transactionDetails;
	}
	public TransactionObject insertTransaction(UserObject user, ArrayList<CartObject> carts) {
		TransactionObject transaction = new TransactionObject();
		sqlConnection.openConnection();
		
		String sql = "SELECT TransactionID FROM transaction_header";
		int size= 0;
		
		String TransactionID = "";
		try {
			statement = sqlConnection.conn.createStatement();
			rs = statement.executeQuery(sql);
			if (rs != null && rs.next()) {
				  rs.last();
				  if(rs.getString("TransactionID").equals("")) {
					  size = Integer.valueOf(rs.getString("TransactionID").substring(2)) + 1; 
				  }
				  if(rs.getRow() > size) {
					  size = rs.getRow() + 1;
				  }
			}
			
			TransactionID = "TH"+String.format("%03d", size);
			System.out.println(TransactionID);
			sql = "INSERT INTO transaction_header (TransactionID, UserID, TransactionStatus,"
					+ "	TransactionDate) VALUES(?, ?, ?, NOW())";
			
			PreparedStatement ps = sqlConnection.conn.prepareStatement(sql);
			ps.setString(1, TransactionID);
			ps.setString(2, user.UserID);
			ps.setString(3, "on going");
			int row = ps.executeUpdate();
			
			transaction.TransactionID = TransactionID;
			transaction.TransactionStatus = "ongoing";
			ArrayList<ProductObject> transactionProducts = new ArrayList<>();
			
			sql = "INSERT INTO transaction_detail (TransactionID, ProductID, ProductQty) VALUES (?, ?, ?)";
			for(int i = 0; i < carts.size(); i++) {
				ps = sqlConnection.conn.prepareStatement(sql);
				ps.setString(1, TransactionID);
				ps.setString(2, carts.get(i).Product.ProductID);
				ps.setInt(3, carts.get(i).Product.ProductQty);
				ps.executeUpdate();
				transactionProducts.add(carts.get(i).Product);
				
				//DELETE PREVIOUS CART
				Carts cartsM = new Carts();
				cartsM.deleteCart(user, carts.get(i).Product);
			
			}
			transaction.transactionProducts = transactionProducts;
		
			
			
			
		}catch(SQLException e) {
			e.printStackTrace();
			System.out.println(e);
		}
			
			
		
		sqlConnection.closeConnection();
		return transaction;
	}
	
}

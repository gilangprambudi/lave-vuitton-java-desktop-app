package com.lavevuitton.store.system;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnection {
	
	public Connection conn = null;
	
	public MySQLConnection() {
		
		
		
	}
	
	public boolean openConnection() {
		String url       = "jdbc:mysql://localhost:3306/ISYS6197?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		String user      = "gilangprambudi";
		String password  = "revenant1234banshee";
		 
		try{
			conn = DriverManager.getConnection(url, user, password);
		} catch(SQLException e) {
		   System.out.println("Database connection not initiated");
		   System.out.println(e.getMessage());
		   return false;
		}finally {
			if(conn != null) {
				System.out.println("Database connection initiated");
				return true;
			}
			
		}
		return false;
	}
	
	public void closeConnection() {
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

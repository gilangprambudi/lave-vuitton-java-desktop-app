package com.lavevuitton.store.system;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class ImagePanel extends JPanel{
	private Image img;
		int width, height;
	  public ImagePanel(int width, int height, Image image) {
	    this(new ImageIcon(image).getImage(), height, width);
	   
	    
	    
	  }


	  
	  public ImagePanel(Image image, int height, int width) {
		  this.img = image;
		  this.width = width;
		    this.height = height;
		  Dimension size = new Dimension(1300, 1080);
		    setPreferredSize(size);
		    setMinimumSize(size);
		    setMaximumSize(size);
		    setSize(size);
		    setLayout(null);
		    System.out.println(height);
		    System.out.println(width);
		    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize(); 
		    this.width = screenSize.width;
		    this.height = screenSize.height;
	  }



	public void paintComponent(Graphics g) {
	    g.drawImage(img, 0, 0,  this.width, this.height, null);
	  }
}

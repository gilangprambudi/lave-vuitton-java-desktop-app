package com.lavevuitton.store.DataObject;

import java.util.Date;

public class UserObject {
	public String UserID;
	public String UserEmail;
	public String UserPassword;
	public Date UserDOB;
	public String UserGender;
	public String UserAddress;
	public String UserRole;
}

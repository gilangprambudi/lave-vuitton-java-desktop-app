package com.lavevuitton.store.DataObject;

import java.sql.Date;
import java.util.ArrayList;

public class TransactionObject {
	public String TransactionID;
	public UserObject UserID;
	public String TransactionStatus;
	public Integer TransactionRating;
	public Date TransactionDate;
	public ArrayList<ProductObject> transactionProducts;
}

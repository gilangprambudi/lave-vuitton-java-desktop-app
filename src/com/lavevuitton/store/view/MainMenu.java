package com.lavevuitton.store.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.table.DefaultTableModel;

import com.lavevuitton.store.DataObject.CartObject;
import com.lavevuitton.store.DataObject.ProductObject;
import com.lavevuitton.store.DataObject.TransactionObject;
import com.lavevuitton.store.DataObject.UserObject;
import com.lavevuitton.store.model.Carts;
import com.lavevuitton.store.model.Products;
import com.lavevuitton.store.model.Transactions;
import com.lavevuitton.store.model.Users;


public class MainMenu  implements ActionListener{
	JFrame 			frame;
	JMenu 			userMenu, viewProductMenu, transactionMenu, manageProductMenu, viewTransactionMenu;  
	JMenuItem 		signInMenu, signUpMenu, signOutMenu, updateProfileMenu, exitMenu, myCartMenu, purchasedProductMenu;
	JMenuBar 		mb;  
	JInternalFrame  signInFrame, signUpFrame, manageProductFrame, viewProductFrame, myCartFrame, updateProfileFrame;
	JDesktopPane    desktopPane;
	JTextField 		signUpEmailTextBox, signInEmailTextBox, mpFrameProductName, mpFrameProductColor, mpFrameProductPrice,
					vpFrameProductID, vpFrameProductName, vpFrameProductColor, vpFrameProductPrice,
					mcFrameProductName, mcFrameProductColor;
	JSpinner		mpFrameProductQty, vpFrameProductQty, mcFrameProductQty;
	JPasswordField  signUpPasswordTextBox, signInPasswordTextBox;
	JTextArea		signUpAddressTextBox;
	JRadioButton	agenderOption, maleOption, femaleOption;
	JComboBox<Integer> dateComboBox, dayComboBox, yearComboBox;
	Boolean			isLoggedIn;
	UserObject		user;
	ProductObject	mpFrameProduct, vpFrameProduct, mcFrameProduct;
	JTable			manageProductTable, viewProductTable, myCartTable;
	DefaultTableModel manageProductTableModel, viewProductTableModel, myCartTableModel;
	JCheckBox 		rememberPassword;
	private BufferedImage img;
	
	
	
	public MainMenu(){
		user = new UserObject();
		initiateUI();
		
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		System.out.println(e.getSource().toString());
		if(e.getSource() == exitMenu) {
			frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
			frame.dispose();
			System.exit(0);
		}else if(e.getSource() == signInMenu) {
			showSignInFrame();
		}else if(e.getSource() == signUpMenu) {
			showSignUpFrame();
		}else if(e.getSource() == myCartMenu) {
			showMyCartFrame();
		}else if(e.getSource() == signOutMenu){
			checkSignOut();
		}else if(e.getSource() == updateProfileMenu) {
			showUpdateProfileFrame();
		}else if(e.getSource() == purchasedProductMenu) {
			JPanel panel = new JPanel();
			 Object[] options1 = { "On Going Transaction(s)", "Finished Transaction(s)" };
		    panel.add(new JLabel("Pick the data you'd like to view"));
		    int result = JOptionPane.showOptionDialog(null, panel, "Enter a Number",
	                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE,
	                null, options1, null);
	        if (result == JOptionPane.YES_OPTION){
	        	showOnGoingTransactionFrame();
	        }else {
	        	showFinishedTransactionFrame();
	        }

		}
	}
	boolean getLoginStatus() {
		try (BufferedReader br = new BufferedReader(new FileReader(new File("login_status.txt")))) {
		    String line;
		    String status = "";
		    String email = "";
		    int x = 0;
		    while ((line = br.readLine()) != null) {
		    	if(x == 0) {
		    		status = line;
		    	}
		    	if(x == 1) {
		    		email = line;
		    	}
		    	x++;
		    }
		    
		    if(status.equals("true")) {
		    	Users userDatabase = new Users();
		    	user = userDatabase.getUserByEmail(email);
		    	if(user != null) {
		    		return true;
		    	}
		    	
		    }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	void checkSignUp() {
		String email 	= signUpEmailTextBox.getText();
		String password = new String(signUpPasswordTextBox.getPassword());
		String gender 	= "";
		String address  = signUpAddressTextBox.getText();
		int    date 	= (Integer) dateComboBox.getSelectedItem();
		int    day 		= (Integer) dayComboBox.getSelectedItem();
		int    year	 	= (Integer) yearComboBox.getSelectedItem();
		
		if(email.equals("") || password.equals("") || address.equals("")) {
			JOptionPane.showMessageDialog(signUpFrame, "All field must be filled", "Alert", JOptionPane.WARNING_MESSAGE);
		}else {
			String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		
			if(!email.matches(regex)) {
				JOptionPane.showMessageDialog(signUpFrame, "Wrong Email format Example: [string]@[domain | must end with .com]", "Alert", JOptionPane.WARNING_MESSAGE);
				
			}else {	
				

				String regex2 = "^[a-zA-Z0-9]*$";
				 
				Pattern pattern = Pattern.compile(regex2);
				
				Matcher match = pattern.matcher(password);
				
				if(match.matches()){
					if(agenderOption.isSelected()) {
						gender = "agender";
					}else if(maleOption.isSelected()){
						gender = "male";
					}else if(femaleOption.isSelected()) {
						gender = "female";
					}else {
						JOptionPane.showMessageDialog(signUpFrame, "Please select your gender!", "Alert", JOptionPane.WARNING_MESSAGE);
					}
					
					if(!gender.equals("")) {
						if(address.length() < 10 || address.length() > 30) {
							JOptionPane.showMessageDialog(signUpFrame, "Address length must be between 10 and 30 long", "Alert", JOptionPane.WARNING_MESSAGE);
						}else {
							UserObject newUser   = new UserObject();
							newUser.UserEmail  	 = email;
							newUser.UserGender 	 = gender;
							newUser.UserPassword = password;
							newUser.UserAddress  = address;
							newUser.UserRole	 = "customer";
							String dateString	 = year+"-"+String.format("%02d", date)+"-"+day;
							try {
								Date userDOB 		 = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
								newUser.UserDOB		 = userDOB;
								Users userDatabase   = new Users();
								newUser 			 = userDatabase.insertUser(newUser);
								
								if(newUser != null) {
									user = newUser;
									JOptionPane.showMessageDialog(signUpFrame, "Hello, "+user.UserEmail.substring(0, user.UserEmail.indexOf("@")) + ", Welcome to Lave's Vuitton Store~");
									signUpFrame.dispose();
									setMenu();
									userDatabase = null;
								}else {
									JOptionPane.showMessageDialog(signUpFrame, "Error on inserting User Data to Database", "Alert", JOptionPane.WARNING_MESSAGE);
								}
								
								
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} 
						}
					}
					
				}else {	
					JOptionPane.showMessageDialog(signUpFrame, "Password can only be Alphanumeric", "Alert", JOptionPane.WARNING_MESSAGE);
				}
				
				
			}
			
		}
		
	}
	
	void checkSignIn() {
		String email 	= signInEmailTextBox.getText();
		String password = new String(signInPasswordTextBox.getPassword());
		
		if(email.equals("") || password.equals("")) {
			JOptionPane.showMessageDialog(signInFrame, "All field cannot be left empty", "Alert", JOptionPane.WARNING_MESSAGE);
		}else {
			Users userDatabase = new Users();
			user = userDatabase.getUserByEmail(email);
			
			if(user.UserID == null) {
				JOptionPane.showMessageDialog(signInFrame, "Wrong email or password", "Alert", JOptionPane.WARNING_MESSAGE);
			}else {
				if(user.UserPassword.equals(password)) {
					signInFrame.dispose();
					setMenu();
					userDatabase = null;
					boolean isPasswordRemembered = rememberPassword.isSelected();
					if(isPasswordRemembered) {
						JOptionPane.showMessageDialog(signInFrame, "Hello, "+user.UserEmail.substring(0, user.UserEmail.indexOf("@")) + ". Password set to be remembered. Next time, you don't have to log in again");	
						File fout = new File("login_status.txt");
						FileOutputStream fos;
						try {
							fos = new FileOutputStream(fout);
							BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
							
								bw.write("true");
								bw.newLine();
								bw.write(user.UserEmail);
								bw.newLine();
						 
							bw.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					 
						
					}else {
						JOptionPane.showMessageDialog(signInFrame, "Hello, "+user.UserEmail.substring(0, user.UserEmail.indexOf("@")));
					}
				}else {
					JOptionPane.showMessageDialog(signInFrame, "Wrong email or password", "Alert", JOptionPane.WARNING_MESSAGE);
				}
				
			}
		}
	}
	
	void checkSignOut() {
		user = new UserObject();
		File fout = new File("login_status.txt");
		FileOutputStream fos;
		try {
			System.out.println("TEST");
			fos = new FileOutputStream(fout);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			
				bw.write("false");
				bw.newLine();
				bw.write("blank");
				bw.newLine();
		 
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setMenu();
		JOptionPane.showMessageDialog(frame, "Sign Out success");
	}
	
	void showSignInFrame() {
		
	    /* Sign In Frame */
	    signInFrame = new JInternalFrame("Sign In", true, true, true, true);
	    signInFrame.setSize(600, 300);
	    signInFrame.getContentPane().setBackground( Color.PINK );
	    signInFrame.setLayout(new GridLayout(4, 1));
	    JLabel signInLabel = new JLabel("Sign In to Lave's Vuitton");
	    signInLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    signInFrame.add(signInLabel);
	    
	    JPanel signInFormPanel = new JPanel();
	    signInFormPanel.setBackground( Color.PINK );
	    signInFormPanel.setBorder(new EmptyBorder(0, 20, 0, 20));
	    signInFormPanel.setLayout(new GridLayout(2, 2));
	    JLabel signInEmailLabel 			= new JLabel("Email");
	    signInEmailTextBox					= new JTextField();
	    JLabel signInPasswordLabel 			= new JLabel("Password");
	    signInPasswordTextBox				= new JPasswordField();
	    signInFormPanel.add(signInEmailLabel);
	    signInFormPanel.add(signInEmailTextBox);
	    signInFormPanel.add(signInPasswordLabel);
	    signInFormPanel.add(signInPasswordTextBox);
	    signInFrame.add(signInFormPanel);	
	    
	    rememberPassword 	= new JCheckBox();
	    JLabel rememberPasswordLabel 	= new JLabel("Remember Password");
	    JPanel rememberPasswordPanel 	= new JPanel();
	    rememberPasswordPanel.setBackground(Color.PINK);
	    rememberPasswordPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
	    rememberPasswordPanel.setBorder(new EmptyBorder(0, 20, 0, 20));
	    rememberPasswordPanel.add(rememberPassword);
	    rememberPasswordPanel.add(rememberPasswordLabel);
	    signInFrame.add(rememberPasswordPanel);
	    
	    JPanel signInButtonPanel = new JPanel();
	    signInButtonPanel.setBackground(Color.PINK);
	    JButton signInButton = new JButton();
	    signInButton.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	            checkSignIn();
	        }
	    });
	    signInButton.setText("Sign In");
	    signInButton.setSize(120, 80);
	    signInButtonPanel.add(signInButton);
	    signInFrame.add(signInButtonPanel);
	    
	    desktopPane.add(signInFrame);
	    
	    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		signInFrame.setLocation(dim.width/2-signInFrame.getSize().width/2, dim.height/2-signInFrame.getSize().height/2);
		signInFrame.setVisible(true);
	    
	}
	
	void showSignUpFrame() {
		/*Sign Up Frame */
	    signUpFrame = new JInternalFrame("Sign Up", true, true, true, true);
	    signUpFrame.setSize(800, 420);
	    
	    signUpFrame.setLayout(new GridLayout(3, 1));
	    JLabel signUpLabel = new JLabel("Sign Up to Lave's Vuitton");
	    signUpLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    signUpFrame.add(signUpLabel);
	    
	    JPanel signUpFormPanel = new JPanel();
	    signUpFormPanel.setBorder(new EmptyBorder(0, 20, 0, 20));
	    signUpFormPanel.setLayout(new GridLayout(5, 2));
	    
	    JLabel signUpEmailLabel 			= new JLabel("Email");
	    signUpEmailTextBox					= new JTextField();
	    
	    JLabel signUpPasswordLabel 			= new JLabel("Password");
	    signUpPasswordTextBox				= new JPasswordField();
	    
	    JLabel signUpBirthdateLabel			= new JLabel("Birthdate");
	    JPanel birthDateFormPanel			= new JPanel();
	    birthDateFormPanel.setLayout(new GridLayout(1, 3));
	    
	    Integer date[] = new Integer[31];
	    for(int i = 0; i < 31; i++) {
	    	date[i] = i+1;
	    }
	    dateComboBox = new JComboBox<>(date);
	    birthDateFormPanel.add(dateComboBox);
	    
	    Integer day[] = new Integer[12];
	    for(int i = 0; i < 12; i++) {
	    	day[i] = i+1;
	    }
	    dayComboBox = new JComboBox<>(day);
	    birthDateFormPanel.add(dayComboBox);
	    
	    Integer year[] = new Integer[63];
	    for(int i = 0; i < 63; i++) {
	    	year[i] = 2003 - i;
	    }
	    yearComboBox = new JComboBox<>(year);
	    birthDateFormPanel.add(yearComboBox);
	    
	    JLabel signUpGenderLabel 			= new JLabel("Gender");
	    JPanel signUpGenderPanel			= new JPanel();
	    signUpGenderPanel.setLayout(new GridLayout(1, 3));
	    
	    JPanel agenderOptionPanel= new JPanel();
	    agenderOption = new JRadioButton();
	    JLabel agenderLabel = new JLabel("Agender");
	    agenderOptionPanel.add(agenderOption);
	    agenderOptionPanel.add(agenderLabel);
	    signUpGenderPanel.add(agenderOptionPanel);
	    
	    JPanel maleOptionPanel= new JPanel();
	    maleOption = new JRadioButton();
	    JLabel maleLabel = new JLabel("Male");
	    maleOptionPanel.add(maleOption);
	    maleOptionPanel.add(maleLabel);
	    signUpGenderPanel.add(maleOptionPanel);
	    
	    JPanel femaleOptionPanel= new JPanel();
	    femaleOption = new JRadioButton();
	    JLabel femaleLabel = new JLabel("Female");
	    femaleOptionPanel.add(femaleOption);
	    femaleOptionPanel.add(femaleLabel);
	    signUpGenderPanel.add(femaleOptionPanel);
	    
	    ButtonGroup genderOptionGroup = new ButtonGroup();
	    genderOptionGroup.add(agenderOption);
	    genderOptionGroup.add(maleOption);
	    genderOptionGroup.add(femaleOption);
	    JLabel signUpAddressLabel 			= new JLabel("Address");
	    signUpAddressTextBox				= new JTextArea();
	    
	    
	    signUpFormPanel.add(signUpEmailLabel);
	    signUpFormPanel.add(signUpEmailTextBox);
	    signUpFormPanel.add(signUpPasswordLabel);
	    signUpFormPanel.add(signUpPasswordTextBox);
	    signUpFormPanel.add(signUpBirthdateLabel);
	    signUpFormPanel.add(birthDateFormPanel);
	    signUpFormPanel.add(signUpGenderLabel);
	    signUpFormPanel.add(signUpGenderPanel);
	    signUpFormPanel.add(signUpAddressLabel);
	    signUpFormPanel.add(signUpAddressTextBox);
	    
	    signUpFrame.add(signUpFormPanel);	
	 
	    
	   
	    
	    JPanel signUpButtonPanel = new JPanel();
	    signUpButtonPanel.setBorder(new EmptyBorder(20, 0, 20, 0));
	    JButton signUpButton = new JButton();
	    signUpButton.setText("Sign Up");
	    signUpButton.setSize(120, 80);
	    signUpButtonPanel.add(signUpButton);
	    signUpFrame.add(signUpButtonPanel);
	    signUpButton.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	            checkSignUp();
	        }
	    });
	    
	    desktopPane.add(signUpFrame);
	    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		signUpFrame.setLocation(dim.width/2-signUpFrame.getSize().width/2, dim.height/2-signUpFrame.getSize().height/2);
		signUpFrame.setVisible(true);
	}
	
	void showUpdateProfileFrame() {
		/* Sign In Frame */
	    updateProfileFrame = new JInternalFrame("Update Profile", true, true, true, true);
	    updateProfileFrame.setSize(600, 300);
	    updateProfileFrame.getContentPane().setBackground( Color.PINK );
	    
	    JPanel updateProfilePanel = new JPanel();
	    updateProfilePanel.setBackground(Color.PINK);
	    BoxLayout boxLayout = new BoxLayout(updateProfilePanel, BoxLayout.Y_AXIS);
	    updateProfilePanel.setLayout(boxLayout);
	    
	    JLabel signInLabel = new JLabel("Update Profile");
	    signInLabel.setBorder(new EmptyBorder(20, 20, 20, 20));
	    signInLabel.setBackground(Color.PINK);
	    updateProfilePanel.add(signInLabel);
	    
	    JPanel updateProfileFormPanel = new JPanel();
	    updateProfileFormPanel.setBackground( Color.PINK );
	    updateProfileFormPanel.setBorder(new EmptyBorder(0, 20, 0, 20));
	    updateProfileFormPanel.setLayout(new GridLayout(4, 2));
	    JLabel signInEmailLabel 			= new JLabel("Email");
	    signInEmailTextBox					= new JTextField();
	    signInEmailTextBox.setText(user.UserEmail);
	    signInEmailTextBox.setEnabled(false);
	    JLabel signInPasswordLabel 			= new JLabel("Password");
	    signInPasswordTextBox				= new JPasswordField();
	    updateProfileFormPanel.add(signInEmailLabel);
	    updateProfileFormPanel.add(signInEmailTextBox);
	    updateProfileFormPanel.add(signInPasswordLabel);
	    updateProfileFormPanel.add(signInPasswordTextBox);
	    
	   
//	    JLabel signUpBirthdateLabel			= new JLabel("Birthdate");
//	    updateProfileFormPanel.add(signUpBirthdateLabel);
//	    JPanel birthDateFormPanel			= new JPanel();
//	    birthDateFormPanel.setLayout(new GridLayout(1, 3));
//	    
//	    Integer date[] = new Integer[31];
//	    for(int i = 0; i < 31; i++) {
//	    	date[i] = i+1;
//	    }
//	    dateComboBox = new JComboBox<>(date);
//	    birthDateFormPanel.add(dateComboBox);
//	    
//	    Integer day[] = new Integer[12];
//	    for(int i = 0; i < 12; i++) {
//	    	day[i] = i+1;
//	    }
//	    dayComboBox = new JComboBox<>(day);
//	    birthDateFormPanel.add(dayComboBox);
//	    
//	    Integer year[] = new Integer[63];
//	    for(int i = 0; i < 63; i++) {
//	    	year[i] = 2003 - i;
//	    }
//	    yearComboBox = new JComboBox<>(year);
//	    birthDateFormPanel.add(yearComboBox);
//	    
//	    updateProfileFormPanel.add(birthDateFormPanel);
	    
	    JLabel signUpGenderLabel 			= new JLabel("Gender");
	    updateProfileFormPanel.add(signUpGenderLabel);
	    JPanel signUpGenderPanel			= new JPanel();
	    signUpGenderPanel.setLayout(new GridLayout(1, 3));
	    
	    JPanel agenderOptionPanel= new JPanel();
	    agenderOption = new JRadioButton();
	    JLabel agenderLabel = new JLabel("Agender");
	    agenderOptionPanel.add(agenderOption);
	    agenderOptionPanel.add(agenderLabel);
	    signUpGenderPanel.add(agenderOptionPanel);
	    
	    JPanel maleOptionPanel= new JPanel();
	    maleOption = new JRadioButton();
	    JLabel maleLabel = new JLabel("Male");
	    maleOptionPanel.add(maleOption);
	    maleOptionPanel.add(maleLabel);
	    signUpGenderPanel.add(maleOptionPanel);
	    
	    JPanel femaleOptionPanel= new JPanel();
	    femaleOption = new JRadioButton();
	    JLabel femaleLabel = new JLabel("Female");
	    femaleOptionPanel.add(femaleOption);
	    femaleOptionPanel.add(femaleLabel);
	    signUpGenderPanel.add(femaleOptionPanel);
	    
	    ButtonGroup genderOptionGroup = new ButtonGroup();
	    genderOptionGroup.add(agenderOption);
	    genderOptionGroup.add(maleOption);
	    genderOptionGroup.add(femaleOption);
	    JLabel signUpAddressLabel 			= new JLabel("Address");
	    signUpAddressTextBox				= new JTextArea();
	    signUpAddressTextBox.setText(user.UserAddress);
	    updateProfileFormPanel.add(signUpGenderPanel);
	    updateProfileFormPanel.add(signUpAddressLabel);
	    updateProfileFormPanel.add(signUpAddressTextBox);
	    
	    
	    
	    
	    JPanel signInButtonPanel = new JPanel();
	    signInButtonPanel.setBackground(Color.PINK);
	    JButton signInButton = new JButton();
	    signInButton.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	            String gender = "";
	            if(!signUpAddressTextBox.getText().equals("") && signInPasswordTextBox.getPassword().length > 0) {
	            	if(agenderOption.isSelected()) {
						gender = "agender";
					}else if(maleOption.isSelected()){
						gender = "male";
					}else if(femaleOption.isSelected()) {
						gender = "female";
					}else {
						JOptionPane.showMessageDialog(updateProfileFrame, "Please select your gender!", "Alert", JOptionPane.WARNING_MESSAGE);
					}
	            	
	            	if(!gender.equals("")) {
	            		user.UserAddress = signUpAddressTextBox.getText();
	            		user.UserGender = gender;
	            		user.UserPassword = String.copyValueOf(signInPasswordTextBox.getPassword());
	            		Users userM = new Users();
	            		boolean userUpdated = userM.updateUser(user);
	            		
	            		if(userUpdated) {
	            			JOptionPane.showMessageDialog(updateProfileFrame, "User data succesfuly updated");
		            		updateProfileFrame.dispose();	
	            		}else {
	            			JOptionPane.showMessageDialog(updateProfileFrame, "User data failed to updated", "Alert", JOptionPane.WARNING_MESSAGE);
	            		}
	            	}
	            }else {
	            	JOptionPane.showMessageDialog(updateProfileFrame, "All forms must be filled", "Alert", JOptionPane.WARNING_MESSAGE);
	            }
	        	
	        }
	    });
	    signInButton.setText("Done");
	    signInButton.setSize(120, 80);
	    signInButtonPanel.add(signInButton);
	    
	    updateProfilePanel.add(updateProfileFormPanel);	
	    updateProfilePanel.add(signInButtonPanel);
	    
	    updateProfileFrame.add(updateProfilePanel);
	    desktopPane.add(updateProfileFrame);
	    
	    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		updateProfileFrame.setLocation(dim.width/2-updateProfileFrame.getSize().width/2, dim.height/2-updateProfileFrame.getSize().height/2);
		updateProfileFrame.setVisible(true);
	}
	void showManageProductFrame() {
		 /* Sign In Frame */
	    manageProductFrame = new JInternalFrame("Manage Product", true, true, true, true);
	    manageProductFrame.setSize(850, 600);
	    
	 // Set the panel to add buttons 
        JPanel panel = new JPanel(); 
  
        // Creating Object of "boxlayout" in  
        // X_Axis from left to right 
        BoxLayout boxlayout = new BoxLayout(panel, BoxLayout.Y_AXIS); 
  
        // to set the box layout 
        panel.setLayout(boxlayout); 
	    
	    JLabel manageProductLabel = new JLabel("Lave's Vuitton Signature");
	    panel.add(manageProductLabel);
	    
	    Products product = new Products();
	    ArrayList<ProductObject> productList = product.getAllProducts();
	    String columnName[] = {"product ID", "Product Name", "ProductPrice", "Product Color", "Product Qty"};
	    String productData[][] = new String[productList.size()][5];
	    for(int i = 0; i < productList.size(); i++) {
	    	ProductObject currentProduct = productList.get(i);
	    	String productNode[] = {currentProduct.ProductID,
	    							currentProduct.ProductName,
	    							currentProduct.ProductPrice.toString(),
	    							currentProduct.ProductColor,
	    							currentProduct.ProductQty.toString()};
	    	productData[i] = productNode;
	    }
	    
	    
	   
	    manageProductTableModel = new DefaultTableModel(productData, columnName);
	    manageProductTable = new JTable();
	    manageProductTable.setModel(manageProductTableModel);
	    
	    JScrollPane sp = new JScrollPane(manageProductTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	    JScrollBar bar = sp.getVerticalScrollBar();
	    bar.setPreferredSize(new Dimension(40, 0));
	    panel.add(sp);
	    
	    JPanel manageProductForm = new JPanel();
	    manageProductForm.setLayout(new GridLayout(5, 2));
	    
	    JLabel nameLabel = new JLabel("Name");
	    manageProductForm.add(nameLabel);
	    mpFrameProductName = new JTextField();
	    manageProductForm.add(mpFrameProductName);
	    
	    JLabel priceLabel = new JLabel("Price");
	    manageProductForm.add(priceLabel);
	    mpFrameProductPrice = new JTextField();
	    manageProductForm.add(mpFrameProductPrice);
	    
	    JLabel qtyLabel = new JLabel("Quantity");
	    manageProductForm.add(qtyLabel);
	    mpFrameProductQty = new JSpinner();
	    manageProductForm.add(mpFrameProductQty);
	    
	    JLabel colorLabel = new JLabel("Color");
	    manageProductForm.add(colorLabel);
	    mpFrameProductColor = new JTextField();
	    manageProductForm.add(mpFrameProductColor);
	    
	    JButton addProductBtn = new JButton("Add Product");
	    manageProductForm.add(addProductBtn);
	    addProductBtn.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	            adminAddProduct();
	        }
	    });
	    
	    
	    JButton deleteProductBtn = new JButton("Delete Product");
	    manageProductForm.add(deleteProductBtn);
	    deleteProductBtn.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	            adminDeleteProduct();
	        }
	    });
	    
	    panel.add(manageProductForm);
	    
	    
	    
	    
	    
	    
	    manageProductFrame.add(panel);
	    
	    
	    manageProductTable.addMouseListener(new MouseAdapter() {
	        public void mousePressed(MouseEvent mouseEvent) {
	            JTable table =(JTable) mouseEvent.getSource();
	            if (mouseEvent.getClickCount() == 1 && table.getSelectedRow() != -1) {
	            	String productID = manageProductTable.getValueAt(table.getSelectedRow(), 0).toString();
					String productName = manageProductTable.getValueAt(table.getSelectedRow(), 1).toString();
					String productPrice = manageProductTable.getValueAt(table.getSelectedRow(), 2).toString();
					String productQty = manageProductTable.getValueAt(table.getSelectedRow(), 4).toString();
					String productColor = manageProductTable.getValueAt(table.getSelectedRow(), 3).toString();	
					
					mpFrameProduct = new ProductObject();
					mpFrameProduct.ProductID = productID;
					
					mpFrameProductName.setText(productName);
					mpFrameProductPrice.setText(productPrice);
					mpFrameProductColor.setText(productColor);
					mpFrameProductQty.setValue(Integer.valueOf(productQty));
	            }
	        }
	    });
	    
	   
	    
	    
	    desktopPane.add(manageProductFrame);
	    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		manageProductFrame.setLocation(dim.width/2-manageProductFrame.getSize().width/2, dim.height/2-manageProductFrame.getSize().height/2);
		manageProductFrame.setVisible(true);
	}
	
	void showViewProductFrame() {
		 /* Sign In Frame */
	    viewProductFrame = new JInternalFrame("View Product", true, true, true, true);
	    viewProductFrame.setSize(850, 600);
	    
	 // Set the panel to add buttons 
        JPanel panel = new JPanel(); 
  
        // Creating Object of "boxlayout" in  
        // X_Axis from left to right 
        BoxLayout boxlayout = new BoxLayout(panel, BoxLayout.Y_AXIS); 
  
        // to set the box layout 
        panel.setLayout(boxlayout); 
	    
	    JLabel viewProductLabel = new JLabel("Lave's Vuitton Signature");
	    panel.add(viewProductLabel);
	    
	    Products product = new Products();
	    ArrayList<ProductObject> productList = product.getAllProducts();
	    String columnName[] = {"product ID", "Product Name", "ProductPrice", "Product Color", "Product Qty"};
	    String productData[][] = new String[productList.size()][5];
	    for(int i = 0; i < productList.size(); i++) {
	    	ProductObject currentProduct = productList.get(i);
	    	String productNode[] = {currentProduct.ProductID,
	    							currentProduct.ProductName,
	    							currentProduct.ProductPrice.toString(),
	    							currentProduct.ProductColor,
	    							currentProduct.ProductQty.toString()};
	    	productData[i] = productNode;
	    }
	    
	    
	   
	    viewProductTableModel = new DefaultTableModel(productData, columnName);
	    viewProductTable = new JTable();
	    viewProductTable.setModel(viewProductTableModel);
	    
	    JScrollPane sp = new JScrollPane(viewProductTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	    JScrollBar bar = sp.getVerticalScrollBar();
	    bar.setPreferredSize(new Dimension(40, 0));
	    panel.add(sp);
	    
	    JPanel viewProductForm = new JPanel();
	    viewProductForm.setLayout(new GridLayout(7, 2));
	    
	    JLabel idLabel = new JLabel("Product ID");
	    viewProductForm.add(idLabel);
	    vpFrameProductID = new JTextField();
	    vpFrameProductID.setEnabled(false);
	    viewProductForm.add(vpFrameProductID);
	    
	    JLabel nameLabel = new JLabel("Name");
	    viewProductForm.add(nameLabel);
	    vpFrameProductName = new JTextField();
	    vpFrameProductName.setEnabled(false);
	    viewProductForm.add(vpFrameProductName);
	    
	    JLabel priceLabel = new JLabel("Price");
	    viewProductForm.add(priceLabel);
	    vpFrameProductPrice = new JTextField();
	    vpFrameProductPrice.setEnabled(false);
	    viewProductForm.add(vpFrameProductPrice);
	    
	    JLabel qtyLabel = new JLabel("Quantity");
	    viewProductForm.add(qtyLabel);
	    vpFrameProductQty = new JSpinner();
	    viewProductForm.add(vpFrameProductQty);
	    
	    JLabel colorLabel = new JLabel("Color");
	    viewProductForm.add(colorLabel);
	    vpFrameProductColor = new JTextField();
	    vpFrameProductColor.setEnabled(false);
	    viewProductForm.add(vpFrameProductColor);
	    
	    JLabel blankLabel = new JLabel("");
	    blankLabel.setVisible(false);
	    viewProductForm.add(blankLabel);
	    
	    JButton addProductBtn = new JButton("Add to Cart");
	    viewProductForm.add(addProductBtn);
	    addProductBtn.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	           addProductToCart();
	        }
	    });
	    
	    
	    
	    panel.add(viewProductForm);
	    
	    
	    viewProductFrame.add(panel);
	    
	    
	    viewProductTable.addMouseListener(new MouseAdapter() {
	        public void mousePressed(MouseEvent mouseEvent) {
	            JTable table =(JTable) mouseEvent.getSource();
	            if (mouseEvent.getClickCount() == 1 && table.getSelectedRow() != -1) {
	            	String productID = viewProductTable.getValueAt(table.getSelectedRow(), 0).toString();
					String productName = viewProductTable.getValueAt(table.getSelectedRow(), 1).toString();
					String productPrice = viewProductTable.getValueAt(table.getSelectedRow(), 2).toString();
					String productQty = viewProductTable.getValueAt(table.getSelectedRow(), 4).toString();
					String productColor = viewProductTable.getValueAt(table.getSelectedRow(), 3).toString();	
					
					vpFrameProduct = new ProductObject();
					vpFrameProduct.ProductID = productID;
					vpFrameProduct.ProductQty = Integer.valueOf(productQty);
					vpFrameProductID.setText(productID);
					vpFrameProductName.setText(productName);
					vpFrameProductPrice.setText(productPrice);
					vpFrameProductColor.setText(productColor);
					vpFrameProductQty.setValue(Integer.valueOf(productQty));
	            }
	        }
	    });
	    
	   
	    
	    
	    desktopPane.add(viewProductFrame);
	    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		viewProductFrame.setLocation(dim.width/2-viewProductFrame.getSize().width/2, dim.height/2-viewProductFrame.getSize().height/2);
		viewProductFrame.setVisible(true);
	}
	
	void showMyCartFrame() {
		/* Sign In Frame */
	    myCartFrame = new JInternalFrame("My Cart", true, true, true, true);
	    myCartFrame.setSize(850, 600);
	    
	 // Set the panel to add buttons 
        JPanel panel = new JPanel(); 
  
        // Creating Object of "boxlayout" in  
        // X_Axis from left to right 
        BoxLayout boxlayout = new BoxLayout(panel, BoxLayout.Y_AXIS); 
  
        // to set the box layout 
        panel.setLayout(boxlayout); 
	    
	    JLabel myCartLabel = new JLabel("My Cart");
	    panel.add(myCartLabel);
	    
	    Carts carts = new Carts();
	    
	    ArrayList<CartObject> cartList = carts.getCartByUserID(user.UserID);
	    String columnName[] = {"product Name", "Product Color", "Product Qty"};
	    String productData[][] = new String[cartList.size()][3];
	    Integer totalPrice = 0;
	    for(int i = 0; i < cartList.size(); i++) {
	    	CartObject currentCart = cartList.get(i);
	    	ProductObject currentProduct = currentCart.Product;
	    	String productNode[] = {currentProduct.ProductName,
	    							currentProduct.ProductColor,
	    							currentProduct.ProductQty.toString()};
	    	totalPrice += currentProduct.ProductQty * currentProduct.ProductPrice;
	    	productData[i] = productNode;
	    }
	    
	    
	   
	    myCartTableModel = new DefaultTableModel(productData, columnName);
	    myCartTable = new JTable();
	    myCartTable.setModel(myCartTableModel);
	    
	    JScrollPane sp = new JScrollPane(myCartTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	    JScrollBar bar = sp.getVerticalScrollBar();
	    bar.setPreferredSize(new Dimension(40, 0));
	    panel.add(sp);
	    
	    JPanel myCartForm = new JPanel();
	    myCartForm.setLayout(new GridLayout(4, 2));
	    
	    
	    JLabel nameLabel = new JLabel("Name");
	    myCartForm.add(nameLabel);
	    mcFrameProductName = new JTextField();
	    mcFrameProductName.setEnabled(false);
	    myCartForm.add(mcFrameProductName);
	    
	    
	    JLabel qtyLabel = new JLabel("Quantity");
	    myCartForm.add(qtyLabel);
	    mcFrameProductQty = new JSpinner();
	    myCartForm.add(mcFrameProductQty);
	    
	    JLabel colorLabel = new JLabel("Color");
	    myCartForm.add(colorLabel);
	    mcFrameProductColor = new JTextField();
	    mcFrameProductColor.setEnabled(false);
	    myCartForm.add(mcFrameProductColor);
	    panel.add(myCartForm);
		    
	   
	    JPanel myCartBtnForm = new JPanel();
	    myCartBtnForm.setLayout(new GridLayout(1, 3));
	    myCartBtnForm.setBorder(new EmptyBorder(20, 0, 20, 0));
	    JButton myCartEditBtn = new JButton("Edit");
	    myCartBtnForm.add(myCartEditBtn);
	   
	    JButton myCartUpdateBtn = new JButton("Update");
	    myCartBtnForm.add(myCartUpdateBtn);
	    JButton myCartCancelBtn = new JButton("Cancel");
	    myCartBtnForm.add(myCartCancelBtn);
	    panel.add(myCartBtnForm);
	  
	    myCartUpdateBtn.setEnabled(false);
    	myCartCancelBtn.setEnabled(false);
    	myCartEditBtn.setEnabled(true);
    	
	    
	    
	    
	    JPanel myCartCheckoutForm = new JPanel();
	    myCartCheckoutForm.setLayout(new GridLayout(1, 2));
	    
	    JLabel totalPriceLabel = new JLabel("Total Price : Rp" + totalPrice.toString());
	    myCartCheckoutForm.add(totalPriceLabel);
	    JButton checkoutBtn = new JButton("Check Out");
	    myCartCheckoutForm.add(checkoutBtn);
	    panel.add(myCartCheckoutForm);
	    
	    checkoutBtn.addActionListener(new ActionListener() {
	    	@Override
	    	public void actionPerformed(ActionEvent e) {
	    		Transactions transaction = new Transactions();
	    		
	    		ArrayList<CartObject> cartList = carts.getCartByUserID(user.UserID);
	    	    transaction.insertTransaction(user, cartList);
	    	    myCartFrame.dispose();
	    	    JOptionPane.showMessageDialog(viewProductFrame, "Checkout Success. Please see transaction menu");
	    	}
	    });
	    
	    myCartEditBtn.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	myCartUpdateBtn.setEnabled(true);
	        	myCartCancelBtn.setEnabled(true);
	        	myCartEditBtn.setEnabled(false);
	        }
	    });
	    
	    myCartCancelBtn.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	myCartUpdateBtn.setEnabled(false);
	        	myCartCancelBtn.setEnabled(false);
	        	myCartEditBtn.setEnabled(true);
	        }
	    });
	    
	    myCartUpdateBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(mcFrameProductName.getText().equals("") || mcFrameProductColor.getText().equals("")) {
					JOptionPane.showMessageDialog(viewProductFrame, "Please Select Product to Update", "Alert", JOptionPane.WARNING_MESSAGE);
				}else {
					Products productM = new Products();
					ProductObject realProduct = productM.getProduct(mcFrameProduct.ProductID);
					
					Integer productQty = Integer.valueOf(mcFrameProductQty.getValue().toString());
					if(realProduct.ProductQty >= (productQty - mcFrameProduct.ProductQty)) {
						mcFrameProduct.ProductQty = productQty;
						
						if(productQty < 0){
							JOptionPane.showMessageDialog(viewProductFrame, "Quantity cannot be lews than 0", "Alert", JOptionPane.WARNING_MESSAGE);
						}else if(productQty == 0) {
							boolean isDeleted = carts.deleteCart(user, mcFrameProduct);
							
							if(isDeleted) {
								ArrayList<CartObject> cartList = carts.getCartByUserID(user.UserID);
							    String columnName[] = {"product Name", "Product Color", "Product Qty"};
							    String productData[][] = new String[cartList.size()][3];
							    Integer totalPrice = 0;
							    for(int i = 0; i < cartList.size(); i++) {
							    	CartObject currentCart = cartList.get(i);
							    	ProductObject currentProduct = currentCart.Product;
							    	String productNode[] = {currentProduct.ProductName,
							    							currentProduct.ProductColor,
							    							currentProduct.ProductQty.toString()};
							    	totalPrice += currentProduct.ProductQty * currentProduct.ProductPrice;
							    	productData[i] = productNode;
							    }
							    
							    myCartTableModel = new DefaultTableModel(productData, columnName);
							    myCartTable.setModel(myCartTableModel);
							    totalPriceLabel.setText(totalPrice+"");
								JOptionPane.showMessageDialog(viewProductFrame, "Your Cart successfuly deleted");
							}else {
								JOptionPane.showMessageDialog(viewProductFrame, "Cart cannot be deleted", "Alert", JOptionPane.WARNING_MESSAGE);
							}
							
						}else {
							carts.updateCart2(user, mcFrameProduct);
							ArrayList<CartObject> cartList = carts.getCartByUserID(user.UserID);
						    String columnName[] = {"product Name", "Product Color", "Product Qty"};
						    String productData[][] = new String[cartList.size()][3];
						    Integer totalPrice = 0;
						    for(int i = 0; i < cartList.size(); i++) {
						    	CartObject currentCart = cartList.get(i);
						    	ProductObject currentProduct = currentCart.Product;
						    	String productNode[] = {currentProduct.ProductName,
						    							currentProduct.ProductColor,
						    							currentProduct.ProductQty.toString()};
						    	totalPrice += currentProduct.ProductQty * currentProduct.ProductPrice;
						    	productData[i] = productNode;
						    }
						    
						    myCartTableModel = new DefaultTableModel(productData, columnName);
						    myCartTable.setModel(myCartTableModel);
						    totalPriceLabel.setText(totalPrice+"");
						    JOptionPane.showMessageDialog(viewProductFrame, "Your Cart successfuly updated");
							
						}
					}else {
						JOptionPane.showMessageDialog(viewProductFrame, "Quantity cannot be more than available product", "Alert", JOptionPane.WARNING_MESSAGE);
					}
					
				}
				
			}
	    	
	    });
	    myCartFrame.add(panel);
	  
	    myCartTable.addMouseListener(new MouseAdapter() {
	        public void mousePressed(MouseEvent mouseEvent) {
	            JTable table =(JTable) mouseEvent.getSource();
	            if (mouseEvent.getClickCount() == 1 && table.getSelectedRow() != -1) {
	            	
	            	String productID = cartList.get(table.getSelectedRow()).Product.ProductID;
					String productName = myCartTable.getValueAt(table.getSelectedRow(), 0).toString();
					String productQty = myCartTable.getValueAt(table.getSelectedRow(), 2).toString();
					String productColor = myCartTable.getValueAt(table.getSelectedRow(), 1).toString();	
					
					mcFrameProduct = new ProductObject();
					mcFrameProduct.ProductID = productID; 
					mcFrameProduct.ProductQty   = Integer.valueOf(productQty);
					mcFrameProduct.ProductColor = productColor;
					mcFrameProduct.ProductName  = productName;
					mcFrameProductName.setText(productName);
					mcFrameProductColor.setText(productColor);
					mcFrameProductQty.setValue(Integer.valueOf(productQty));
	            }
	        }
	    });
	    
	   
	    
	    
	    desktopPane.add(myCartFrame);
	    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		myCartFrame.setLocation(dim.width/2-myCartFrame.getSize().width/2, dim.height/2-myCartFrame.getSize().height/2);
		myCartFrame.setVisible(true);
	}
	
	void showOnGoingTransactionFrame() {
		/* Sign In Frame */
	    JInternalFrame onGoingTransactionFrame = new JInternalFrame("Transaction", true, true, true, true);
	    onGoingTransactionFrame.setSize(850, 300);
	    
	    TransactionObject selectedTransaction = new TransactionObject();
	 // Set the panel to add buttons 
        JPanel panel = new JPanel(); 
  
        // Creating Object of "boxlayout" in  
        // X_Axis from left to right 
        BoxLayout boxlayout = new BoxLayout(panel, BoxLayout.Y_AXIS); 
  
        // to set the box layout 
        panel.setLayout(boxlayout); 
	    
	    JLabel myCartLabel = new JLabel("On Going Transaction");
	    myCartLabel.setHorizontalAlignment(JLabel.CENTER);		
	    panel.add(myCartLabel);
	    
	    Transactions transactionM = new Transactions();

	    ArrayList<TransactionObject> transactionList = transactionM.getUserTransaction(user, "on going");
	    
	    String columnName[] = {"Transaction ID", "Transaction Date", "Total Price", "Status"};
	    String transactionData[][] = new String[transactionList.size()][4];
	    Integer totalPrice = 0;
	    for(int i = 0; i < transactionList.size(); i++) {
	    	totalPrice = 0;
	    	for(int y = 0; y < transactionList.get(i).transactionProducts.size(); y++) {
	    		totalPrice += transactionList.get(i).transactionProducts.get(y).ProductPrice;
	    	}
	    	System.out.println(transactionList.get(i).TransactionID);
	    	String transactionNode[] = {transactionList.get(i).TransactionID,
	    								transactionList.get(i).TransactionDate.toString(),
	    								totalPrice+"",
	    								transactionList.get(i).TransactionStatus};
	    	transactionData[i] = transactionNode;
	    }
	    
	    
	   
	    DefaultTableModel transactionModel = new DefaultTableModel(transactionData, columnName);
	    JTable transactionTable = new JTable();
	    transactionTable.setModel(transactionModel);
	    
	    JScrollPane sp = new JScrollPane(transactionTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	    JScrollBar bar = sp.getVerticalScrollBar();
	    bar.setPreferredSize(new Dimension(40, 0));
	    panel.add(sp);
	    
	    JPanel transactionForm = new JPanel();
	    transactionForm.setBorder(new EmptyBorder(20, 20, 20, 20));
	    transactionForm.setLayout(new GridLayout(1, 3));
	    JLabel rateOrderLabel = new JLabel("Rate Transaction");
	    transactionForm.add(rateOrderLabel);
	    JSlider rateSlider = new JSlider(1, 5, 1);
	    // Add positions label in the slider
	    Hashtable<Integer, JLabel> position = new Hashtable();
	    position.put(1, new JLabel("1"));
	    position.put(2, new JLabel("2"));
	    position.put(3, new JLabel("3"));
	    position.put(4, new JLabel("4"));
	    position.put(5, new JLabel("5"));
	    rateSlider.setPaintLabels(true);
	    rateSlider.setLabelTable(position); 
	    transactionForm.add(rateSlider);
	    JButton submitBtn = new JButton("Submit");
	    transactionForm.add(submitBtn);
	    rateOrderLabel.setVisible(false);
	    rateSlider.setVisible(false);
	    submitBtn.setVisible(false);
	    
	    submitBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Transactions transaction = new Transactions();
				int rating = rateSlider.getValue();
				boolean transactionStatus = transaction.updateTransactionStatus(selectedTransaction.TransactionID, rating);
				if(transactionStatus) {
					JOptionPane.showMessageDialog(onGoingTransactionFrame, "Your transaction succesfuly updated. Thank You for your shopping");
					onGoingTransactionFrame.dispose();
				}else {
					JOptionPane.showMessageDialog(onGoingTransactionFrame, "Error updating to database", "Alert", JOptionPane.WARNING_MESSAGE);
					
				}
			}
	    	
	    });
	    
	    panel.add(transactionForm);
	    onGoingTransactionFrame.add(panel);
	    
	    transactionTable.addMouseListener(new MouseAdapter() {
	        public void mousePressed(MouseEvent mouseEvent) {
	            JTable table =(JTable) mouseEvent.getSource();
	            if (mouseEvent.getClickCount() == 1 && table.getSelectedRow() != -1) {
	            	String transactionID = transactionTable.getValueAt(table.getSelectedRow(), 0).toString();
					selectedTransaction.TransactionID = transactionID;
					int dialogButton = JOptionPane.showConfirmDialog (onGoingTransactionFrame, "Have you received the order for transaction "+transactionID +"?","Fransaction Confirmation", JOptionPane.YES_NO_OPTION);
					
					if(dialogButton == JOptionPane.YES_OPTION) {
						JOptionPane.showMessageDialog(onGoingTransactionFrame, "Please Rate your order");
						rateOrderLabel.setText("Rate Transaction for : "+transactionID);
						rateOrderLabel.setVisible(true);
						rateSlider.setVisible(true);
						submitBtn.setVisible(true);
					}
	            }
	        }
	    });
	    
	    desktopPane.add(onGoingTransactionFrame);
	    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		onGoingTransactionFrame.setLocation(dim.width/2-onGoingTransactionFrame.getSize().width/2, dim.height/2-onGoingTransactionFrame.getSize().height/2);
		onGoingTransactionFrame.setVisible(true);
	}

	void showFinishedTransactionFrame() {
	    JInternalFrame onGoingTransactionFrame = new JInternalFrame("Transaction", true, true, true, true);
	    onGoingTransactionFrame.setSize(850, 300);
	    
		JPanel panel = new JPanel(); 
		  
        // Creating Object of "boxlayout" in  
        // X_Axis from left to right 
        BoxLayout boxlayout = new BoxLayout(panel, BoxLayout.Y_AXIS); 
  
        // to set the box layout 
        panel.setLayout(boxlayout); 
	    
	    JLabel myCartLabel = new JLabel("Finished Transaction");
	    myCartLabel.setHorizontalAlignment(JLabel.CENTER);		
	    panel.add(myCartLabel);
	    
	    Transactions transactionM = new Transactions();
	    
	    ArrayList<TransactionObject> transactionList = transactionM.getUserTransaction(user, "success");
	    
	    String columnName[] = {"Transaction ID", "Transaction Date", "Total Price", "Rating"};
	    String transactionData[][] = new String[transactionList.size()][4];
	    Integer totalPrice = 0;
	    for(int i = 0; i < transactionList.size(); i++) {
	    	totalPrice = 0;
	    	for(int y = 0; y < transactionList.get(i).transactionProducts.size(); y++) {
	    		totalPrice += transactionList.get(i).transactionProducts.get(y).ProductPrice;
	    	}
	    	System.out.println(transactionList.get(i).TransactionID);
	    	String transactionNode[] = {transactionList.get(i).TransactionID,
	    								transactionList.get(i).TransactionDate.toString(),
	    								totalPrice+"",
	    								transactionList.get(i).TransactionRating+""};
	    	transactionData[i] = transactionNode;
	    }
	    
	    
	   
	    DefaultTableModel transactionModel = new DefaultTableModel(transactionData, columnName);
	    JTable transactionTable = new JTable();
	    transactionTable.setModel(transactionModel);
	    
	    JScrollPane sp = new JScrollPane(transactionTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	    JScrollBar bar = sp.getVerticalScrollBar();
	    bar.setPreferredSize(new Dimension(40, 0));
	    panel.add(sp);
	    onGoingTransactionFrame.add(panel);
	    desktopPane.add(onGoingTransactionFrame);
	    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		onGoingTransactionFrame.setLocation(dim.width/2-onGoingTransactionFrame.getSize().width/2, dim.height/2-onGoingTransactionFrame.getSize().height/2);
		onGoingTransactionFrame.setVisible(true);
	    
	}
	
	void addProductToCart() {
		String currentProductID = null;
		if(vpFrameProduct != null) {
			if(vpFrameProduct.ProductID != null) {
				currentProductID = vpFrameProduct.ProductID;
			}
		}else {
			vpFrameProduct = new ProductObject();
		}
		
		if(currentProductID != null) {
			vpFrameProduct.ProductID = currentProductID;
		}
		
		String productName  = vpFrameProductName.getText();
		String productColor = vpFrameProductColor.getText();
		
		if(!productName.equals("") && 
		   !productColor.equals("") &&
		   !vpFrameProductQty.getValue().toString().equals("") &&
		   !vpFrameProductPrice.getText().equals("") &&
		   Integer.valueOf(vpFrameProductQty.getValue().toString()) > 0) {
			   Integer productQty   = Integer.valueOf(vpFrameProductQty.getValue().toString());
			   if(productQty <= vpFrameProduct.ProductQty) {
			   Integer productPrice = Integer.valueOf(vpFrameProductPrice.getText());
				vpFrameProduct.ProductName = productName;
				vpFrameProduct.ProductPrice = productPrice;
				vpFrameProduct.ProductQty = productQty;
				vpFrameProduct.ProductColor = productColor;
				
				Carts cart = new Carts();
				
				Integer cartInserted = cart.insertOrUpdateCart(user, vpFrameProduct);
				
				if(cartInserted == 1) {
					viewProductTableModel.setRowCount(0);
					Products product = new Products();
				    ArrayList<ProductObject> productList = product.getAllProducts();
				    String columnName[] = {"product ID", "Product Name", "ProductPrice", "Product Color", "Product Qty"};
				    String productData[][] = new String[productList.size()][5];
				    for(int i = 0; i < productList.size(); i++) {
				    	ProductObject currentProduct = productList.get(i);
				    	String productNode[] = {currentProduct.ProductID,
				    							currentProduct.ProductName,
				    							currentProduct.ProductPrice.toString(),
				    							currentProduct.ProductColor,
				    							currentProduct.ProductQty.toString()};
				    	productData[i] = productNode;
				    }
				   
				    viewProductTableModel = new DefaultTableModel(productData, columnName);
				    viewProductTable.setModel(viewProductTableModel);
				    vpFrameProductName.setText("");
				    vpFrameProductID.setText("");
				    vpFrameProductPrice.setText("");
				    vpFrameProductQty.setValue(0);
				    vpFrameProductColor.setText("");
				    JOptionPane.showMessageDialog(viewProductFrame, "New product successfuly added");
				}else if(cartInserted == 2) {
					Products product = new Products();
					ArrayList<ProductObject> productList = product.getAllProducts();
				    String columnName[] = {"product ID", "Product Name", "ProductPrice", "Product Color", "Product Qty"};
				    String productData[][] = new String[productList.size()][5];
				    for(int i = 0; i < productList.size(); i++) {
				    	ProductObject currentProduct = productList.get(i);
				    	String productNode[] = {currentProduct.ProductID,
				    							currentProduct.ProductName,
				    							currentProduct.ProductPrice.toString(),
				    							currentProduct.ProductColor,
				    							currentProduct.ProductQty.toString()};
				    	productData[i] = productNode;
				    }
				   
				    viewProductTableModel = new DefaultTableModel(productData, columnName);
				    viewProductTable.setModel(viewProductTableModel);
				    JOptionPane.showMessageDialog(viewProductFrame, "Your Cart successfuly updated");
				    vpFrameProductName.setText("");
				    vpFrameProductID.setText("");
				    vpFrameProductPrice.setText("");
				    vpFrameProductQty.setValue(0);
				    vpFrameProductColor.setText("");
				}else {
					JOptionPane.showMessageDialog(viewProductFrame, "Error adding to database", "Alert", JOptionPane.WARNING_MESSAGE);
				}
			}else {
				JOptionPane.showMessageDialog(viewProductFrame, "Product Quantity cannot be more than available product", "Alert", JOptionPane.WARNING_MESSAGE);
			}
		}else {
			JOptionPane.showMessageDialog(viewProductFrame, "All field must be filled. Number cannot be less than 0", "Alert", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	
	
	
	
	void initiateUI() {
		isLoggedIn	= getLoginStatus();
		frame = new JFrame("Lave Vuitton Store");
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		frame.setUndecorated(false);
		frame.setVisible(true);
		//ImagePanel panel = new ImagePanel(frame.getSize().width, frame.getSize().height, new ImageIcon("bg-image.jpg").getImage());

		
		
		setMenu();
		//frame.getContentPane().add(panel);	
		
		
		 try {
            img = ImageIO.read(new File("bg-image.jpg"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
	    desktopPane = new JDesktopPane() {
	    	  @Override
	            protected void paintComponent(Graphics grphcs) {
	                super.paintComponent(grphcs);
	                grphcs.drawImage(img, 0, 0, frame.getWidth(), frame.getHeight(), null);
	                
	            }

	            @Override
	            public Dimension getPreferredSize() {
	                return new Dimension(1300, 800);
	            }
	    };
	    
	    
	    frame.add(desktopPane);
	    
	    frame.pack();
	    
	    
	    frame.setVisible(true);
	}
	
	void adminAddProduct() {
		String currentProductID = null;
		if(mpFrameProduct != null) {
			if(mpFrameProduct.ProductID != null) {
				currentProductID = mpFrameProduct.ProductID;
			}
		}
		
		mpFrameProduct = new ProductObject();
		
		if(currentProductID != null) {
			mpFrameProduct.ProductID = currentProductID;
		}
		
		String productName  = mpFrameProductName.getText();
		String productColor = mpFrameProductColor.getText();
		
		
		if(!productName.equals("") && 
		   !productColor.equals("") &&
		   !mpFrameProductQty.getValue().toString().equals("") &&
		   !mpFrameProductPrice.getText().equals("")) {
		   Integer productQty   = Integer.valueOf(mpFrameProductQty.getValue().toString());
		   Integer productPrice = Integer.valueOf(mpFrameProductPrice.getText());
			mpFrameProduct.ProductName = productName;
			mpFrameProduct.ProductPrice = productPrice;
			mpFrameProduct.ProductQty = productQty;
			mpFrameProduct.ProductColor = productColor;
			
			Products product = new Products();
			
			Integer productInserted = product.addOrUpdateProduct(mpFrameProduct);
			if(productInserted == 1) {
				manageProductTableModel.setRowCount(0);
			    ArrayList<ProductObject> productList = product.getAllProducts();
			    String columnName[] = {"product ID", "Product Name", "ProductPrice", "Product Color", "Product Qty"};
			    String productData[][] = new String[productList.size()][5];
			    for(int i = 0; i < productList.size(); i++) {
			    	ProductObject currentProduct = productList.get(i);
			    	String productNode[] = {currentProduct.ProductID,
			    							currentProduct.ProductName,
			    							currentProduct.ProductPrice.toString(),
			    							currentProduct.ProductColor,
			    							currentProduct.ProductQty.toString()};
			    	productData[i] = productNode;
			    }
			   
			    manageProductTableModel = new DefaultTableModel(productData, columnName);
			    manageProductTable.setModel(manageProductTableModel);
			    JOptionPane.showMessageDialog(signUpFrame, "New product successfuly added");
			}else if(productInserted == 2) {
				ArrayList<ProductObject> productList = product.getAllProducts();
			    String columnName[] = {"product ID", "Product Name", "ProductPrice", "Product Color", "Product Qty"};
			    String productData[][] = new String[productList.size()][5];
			    for(int i = 0; i < productList.size(); i++) {
			    	ProductObject currentProduct = productList.get(i);
			    	String productNode[] = {currentProduct.ProductID,
			    							currentProduct.ProductName,
			    							currentProduct.ProductPrice.toString(),
			    							currentProduct.ProductColor,
			    							currentProduct.ProductQty.toString()};
			    	productData[i] = productNode;
			    }
			   
			    manageProductTableModel = new DefaultTableModel(productData, columnName);
			    manageProductTable.setModel(manageProductTableModel);
			    JOptionPane.showMessageDialog(signUpFrame, "Current Product already updated");
			}else {
				JOptionPane.showMessageDialog(signUpFrame, "Error adding to database", "Alert", JOptionPane.WARNING_MESSAGE);
			}
		}else {
			JOptionPane.showMessageDialog(manageProductFrame, "All field must be filled. Number cannot be less than 0", "Alert", JOptionPane.WARNING_MESSAGE);
		}
		
	}
	
	void adminDeleteProduct() {
		String currentProductID = null;
		if(mpFrameProduct != null) {
			if(mpFrameProduct.ProductID != null) {
				currentProductID = mpFrameProduct.ProductID;
			}
		}
		
		mpFrameProduct = new ProductObject();
		
		if(currentProductID != null) {
			mpFrameProduct.ProductID = currentProductID;
		}
		
		String productName  = mpFrameProductName.getText();
		String productColor = mpFrameProductColor.getText();
		
		if(!productName.equals("") && 
		   !productColor.equals("")) {
			
			mpFrameProduct.ProductName = productName;
			mpFrameProduct.ProductColor = productColor;
			
			Products product = new Products();
			
			boolean productDeleted = product.deleteProduct(mpFrameProduct);
			if(productDeleted) {
				manageProductTableModel.setRowCount(0);
			    ArrayList<ProductObject> productList = product.getAllProducts();
			    String columnName[] = {"product ID", "Product Name", "ProductPrice", "Product Color", "Product Qty"};
			    String productData[][] = new String[productList.size()][5];
			    for(int i = 0; i < productList.size(); i++) {
			    	ProductObject currentProduct = productList.get(i);
			    	String productNode[] = {currentProduct.ProductID,
			    							currentProduct.ProductName,
			    							currentProduct.ProductPrice.toString(),
			    							currentProduct.ProductColor,
			    							currentProduct.ProductQty.toString()};
			    	productData[i] = productNode;
			    }
			   
			    manageProductTableModel = new DefaultTableModel(productData, columnName);
			    manageProductTable.setModel(manageProductTableModel);
			    JOptionPane.showMessageDialog(signUpFrame, "Product succesfuly deleted");
			}else {
				JOptionPane.showMessageDialog(signUpFrame, "No Product deleted", "Alert", JOptionPane.WARNING_MESSAGE);
			}
		}else {
			JOptionPane.showMessageDialog(manageProductFrame, "All field must be filled.", "Alert", JOptionPane.WARNING_MESSAGE);
		}
		
	}
	
	void adminViewTransactionFrame(){
		 JInternalFrame onGoingTransactionFrame = new JInternalFrame("Transaction", true, true, true, true);
		    onGoingTransactionFrame.setSize(850, 300);
		    
			JPanel panel = new JPanel(); 
			  
	        // Creating Object of "boxlayout" in  
	        // X_Axis from left to right 
	        BoxLayout boxlayout = new BoxLayout(panel, BoxLayout.Y_AXIS); 
	  
	        // to set the box layout 
	        panel.setLayout(boxlayout); 
		    
		    JLabel myCartLabel = new JLabel("Transaction History");
		    myCartLabel.setHorizontalAlignment(JLabel.CENTER);		
		    panel.add(myCartLabel);
		    
		    Transactions transactionM = new Transactions();
		    
		    ArrayList<TransactionObject> transactionList = transactionM.getUserTransaction(null, "success");
		    
		    String columnName[] = {"Transaction ID", "Transaction Date", "Total Price", "Rating"};
		    String transactionData[][] = new String[transactionList.size()][4];
		    Integer totalPrice = 0;
		    
		    System.out.println(transactionList.size());
		    for(int i = 0; i < transactionList.size(); i++) {
		    	totalPrice = 0;
		    	for(int y = 0; y < transactionList.get(i).transactionProducts.size(); y++) {
		    		totalPrice += transactionList.get(i).transactionProducts.get(y).ProductPrice;
		    	}
		    	System.out.println(transactionList.get(i).TransactionID);
		    	String transactionNode[] = {transactionList.get(i).TransactionID,
		    								transactionList.get(i).TransactionDate.toString(),
		    								totalPrice+"",
		    								transactionList.get(i).TransactionRating+""};
		    	transactionData[i] = transactionNode;
		    }
		    
		    
		   
		    DefaultTableModel transactionModel = new DefaultTableModel(transactionData, columnName);
		    JTable transactionTable = new JTable();
		    transactionTable.setModel(transactionModel);
		    
		    JScrollPane sp = new JScrollPane(transactionTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		    JScrollBar bar = sp.getVerticalScrollBar();
		    bar.setPreferredSize(new Dimension(40, 0));
		    panel.add(sp);
		    onGoingTransactionFrame.add(panel);
		    desktopPane.add(onGoingTransactionFrame);
		    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			onGoingTransactionFrame.setLocation(dim.width/2-onGoingTransactionFrame.getSize().width/2, dim.height/2-onGoingTransactionFrame.getSize().height/2);
			onGoingTransactionFrame.setVisible(true);
	}
	void setMenu() {
		frame.setJMenuBar(null);
		
		mb 					= new JMenuBar();
        mb.revalidate();
		/*Declaration of User Menu */
	    userMenu			= new JMenu("User");      
	    signInMenu 			= new JMenuItem("Sign In");
	    signInMenu.addActionListener(this);
	    signUpMenu 			= new JMenuItem("Sign Up");
	    signUpMenu.addActionListener(this);
	    signOutMenu			= new JMenuItem("Sign Out");
	    signOutMenu.addActionListener(this);
	    updateProfileMenu 	= new JMenuItem("Update Profile");
	    updateProfileMenu.addActionListener(this);
	    
	    exitMenu 			= new JMenuItem("Exit");
	    exitMenu.addActionListener(this);
	    
	    //Adding sub-menu items to UserMenu 
	    userMenu.add(signInMenu);
	    userMenu.add(signUpMenu);
	    userMenu.add(updateProfileMenu);
	    userMenu.add(signOutMenu);
	    userMenu.add(exitMenu);
	    
	    
	    mb.add(userMenu); 
	   
		
	    if(user.UserRole != null) {
	    	
		    
		    updateProfileMenu.setEnabled(true);
		    signOutMenu.setEnabled(true);
		    
		    signInMenu.setEnabled(false);
		    signUpMenu.setEnabled(false);
	    	
	    	if(user.UserRole.equals("admin")) {
	    		manageProductMenu	= new JMenu("Manage Product");
	    		manageProductMenu.addMenuListener(new MenuListener() {

					@Override
					public void menuSelected(MenuEvent e) {
						
						showManageProductFrame();
						
					}

					@Override
					public void menuDeselected(MenuEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void menuCanceled(MenuEvent e) {
						// TODO Auto-generated method stub
						
					}
	    			
	    		});
	    		
	    		viewTransactionMenu = new JMenu("View Transaction History");
	    		viewTransactionMenu.addMenuListener(new MenuListener() {

					@Override
					public void menuSelected(MenuEvent e) {
						adminViewTransactionFrame();
						
					}

					@Override
					public void menuDeselected(MenuEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void menuCanceled(MenuEvent e) {
						// TODO Auto-generated method stub
						
					}
	    			
	    		});
	    			    		
	    		mb.add(manageProductMenu);
	    		mb.add(viewTransactionMenu);
	    	}else {
	    		viewProductMenu		 = new JMenu("View Products");
	    		viewProductMenu.addMenuListener(new MenuListener() {

					@Override
					public void menuSelected(MenuEvent e) {
						showViewProductFrame();
						
					}

					@Override
					public void menuDeselected(MenuEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void menuCanceled(MenuEvent e) {
						// TODO Auto-generated method stub
						
					}
	    			
	    		});
	    		mb.add(viewProductMenu);
	    		
	    		transactionMenu		 = new JMenu("Transactions");
	    		myCartMenu			 = new JMenuItem("My Cart");
	    		myCartMenu.addActionListener(this);
	    		purchasedProductMenu = new JMenuItem("Purchased Products");
	    		purchasedProductMenu.addActionListener(this);
	    		transactionMenu.add(myCartMenu);
	    		transactionMenu.add(purchasedProductMenu);
	    		mb.add(transactionMenu);
	    	}
	    }else {
	    	
	    	signOutMenu.setEnabled(false);
	    	updateProfileMenu.setEnabled(false);
	    	
	    	
	    }
	    
	    
	    
	    frame.setJMenuBar(mb); 
	    frame.repaint();
		frame.revalidate();
	}
	
	
	
}
